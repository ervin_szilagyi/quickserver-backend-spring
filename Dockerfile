FROM openjdk:11-jdk-oracle

VOLUME /tmp

COPY /target/quickserver-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=production", "-jar", "/app.jar"]