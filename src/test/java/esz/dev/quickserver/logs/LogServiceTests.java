package esz.dev.quickserver.logs;

import esz.dev.quickserver.logging.dto.LogEntryConverter;
import esz.dev.quickserver.logging.dto.LogEntryDto;
import esz.dev.quickserver.logging.model.Level;
import esz.dev.quickserver.logging.model.LogEntry;
import esz.dev.quickserver.logging.repository.LogRepository;
import esz.dev.quickserver.logging.service.LogService;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@DisplayName("LogService tests")
public class LogServiceTests {
    @InjectMocks
    private LogService logService;

    @Mock
    private LogRepository logRepositoryMock;

    @Mock
    private LogEntryConverter logEntryConverterMock;

    @Captor
    private ArgumentCaptor<LogEntry> captor;

    @Before
    public void beforeEach() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("Should create an INFO level log entry.")
    public void shouldLogInfo() {
        String message = "info test";

        logService.info(message);

        Mockito.verify(logRepositoryMock, Mockito.times(1)).insert(captor.capture());
        Assertions.assertThat(captor.getValue().getLevel()).isEqualByComparingTo(Level.INFO);
        Assertions.assertThat(captor.getValue().getMessage()).isEqualToIgnoringCase(message);
    }

    @Test
    @DisplayName("Should create an WARNING level log entry.")
    public void shouldLogWarning() {
        String message = "warning test";

        logService.warning(message);

        Mockito.verify(logRepositoryMock, Mockito.times(1)).insert(captor.capture());
        Assertions.assertThat(captor.getValue().getLevel()).isEqualByComparingTo(Level.WARNING);
        Assertions.assertThat(captor.getValue().getMessage()).isEqualToIgnoringCase(message);
    }

    @Test
    @DisplayName("Should create an ERROR level log entry.")
    public void shouldLogError() {
        String message = "error test";

        logService.error(message);

        Mockito.verify(logRepositoryMock, Mockito.times(1)).insert(captor.capture());
        Assertions.assertThat(captor.getValue().getLevel()).isEqualByComparingTo(Level.ERROR);
        Assertions.assertThat(captor.getValue().getMessage()).isEqualToIgnoringCase(message);
    }

    @Test
    @DisplayName("Should create an CRITICAL level log entry.")
    public void shouldLogCritical() {
        String message = "critical test";

        logService.critical(message);

        Mockito.verify(logRepositoryMock, Mockito.times(1)).insert(captor.capture());
        Assertions.assertThat(captor.getValue().getLevel()).isEqualByComparingTo(Level.CRITICAL);
        Assertions.assertThat(captor.getValue().getMessage()).isEqualToIgnoringCase(message);
    }

    @Test
    @DisplayName("Should return first page of log entries.")
    public void shouldReturnFirstPageOfLogEntries() {
        int numberOfEntries = 90;
        ArrayList<LogEntry> entries = generateEntries(numberOfEntries);
        int entriesPerPage = 100;
        int page = 1;
        Mockito.when(logRepositoryMock.findAllByOrderByCreationDateDesc(PageRequest.of((page - 1) * entriesPerPage, entriesPerPage)))
                .thenReturn(new PageImpl<>(entries.subList(page - 1, numberOfEntries)));
        Mockito.when(logEntryConverterMock.createFromEntities(entries)).thenReturn(
                entries.stream()
                        .map(entry -> new LogEntryDto(entry.getId(), entry.getCreationDate(), entry.getLevel(), entry.getMessage()))
                        .collect(Collectors.toList()));

        List<LogEntryDto> actual = logService.getLogsByPage(page);
        Assertions.assertThat(numberOfEntries).isEqualTo(actual.size());
        Mockito.verify(logRepositoryMock, Mockito.times(1)).findAllByOrderByCreationDateDesc(PageRequest.of(page - 1, entriesPerPage));
        Mockito.verifyNoMoreInteractions(logRepositoryMock);
    }

    private ArrayList<LogEntry> generateEntries(int count) {
        ArrayList<LogEntry> entries = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            String entryId = "entry-id-" + i;
            Date date = new GregorianCalendar(2018, 5, 27, 12, 59, 59).getTime();
            String message = "message-" + i;
            entries.add(new LogEntry(entryId, date, Level.INFO, message));
        }
        return entries;
    }
}


