package esz.dev.quickserver.blogEntry;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import esz.dev.quickserver.blogEntry.dto.EntryDto;
import esz.dev.quickserver.blogEntry.service.EntryService;
import esz.dev.quickserver.blogEntry.service.StorageService;
import esz.dev.quickserver.blogEntry.validation.EntryValidationErrorCodes;
import esz.dev.quickserver.logging.service.LogService;
import esz.dev.quickserver.security.jwt.JwtTokenUtil;
import esz.dev.quickserver.security.jwt.service.JwtUserDetailsService;
import esz.dev.quickserver.users.dto.UserDto;
import esz.dev.quickserver.users.model.Authority;
import esz.dev.quickserver.users.validation.UserValidationErrorCodes;
import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application.yml")
@EnableConfigurationProperties
@DisplayName("EntryController tests")
public class EntryControllerTests {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    public EntryService entryService;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @MockBean
    private LogService logService;

    @MockBean
    private StorageService storageService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @DisplayName("Should get entries on the first page.")
    public void shouldGetEntriesByFirstPage() throws Exception {
        UserDto userDto1 = new UserDto("uid", "username", Stream.of(new Authority[]{Authority.ROLE_USER}).collect(Collectors.toList()));
        EntryDto entryDto1 = new EntryDto("id1", "title", "summary", "content", new Date(), userDto1);
        EntryDto entryDto2 = new EntryDto("id2", "title", "summary", "content", new Date(), userDto1);
        EntryDto entryDto3 = new EntryDto("id3", "title", "summary", "content", new Date(), userDto1);

        ArrayList<EntryDto> entryList = new ArrayList<>() {{
            add(entryDto1);
            add(entryDto2);
            add(entryDto3);
        }};
        Mockito.when(entryService.getEntriesByPage(1)).thenReturn(entryList);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/entries/page/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id").value("id1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[1].id").value("id2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[2].id").value("id3"));
    }

    @Test
    @DisplayName("Should get the entry with a given ID.")
    public void shouldGetEntryWithId() throws Exception {
        UserDto userDto = new UserDto("uid", "username", Stream.of(new Authority[]{Authority.ROLE_USER}).collect(Collectors.toList()));
        EntryDto entryDto = new EntryDto("id1", "title", "summary", "content", new Date(), userDto);
        Mockito.when(entryService.getEntryDtoById("id1")).thenReturn(entryDto);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/entries/id1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("id").value("id1"))
                .andExpect(MockMvcResultMatchers.jsonPath("title").value("title"))
                .andExpect(MockMvcResultMatchers.jsonPath("summary").value("summary"))
                .andExpect(MockMvcResultMatchers.jsonPath("content").value("content"));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("Should update an entry.")
    public void shouldUpdateEntry() throws Exception {
        UserDto userDto = new UserDto("uid", "username", Stream.of(new Authority[]{Authority.ROLE_USER}).collect(Collectors.toList()));
        EntryDto entryDto = new EntryDto("id1", "title", "summary", "content", new Date(), userDto);
        Mockito.doNothing().when(entryService).updateEntry(entryDto);
        mockMvc.perform(MockMvcRequestBuilders.put("/api/entries/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(entryDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("Should insert an entry.")
    public void shouldInsertEntry() throws Exception {
        UserDto userDto = new UserDto("uid", "username", Stream.of(new Authority[]{Authority.ROLE_USER}).collect(Collectors.toList()));
        EntryDto entryDto = new EntryDto("id1", "title", "summary", "content", new Date(), userDto);
        Mockito.doNothing().when(entryService).addEntry(entryDto);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/entries/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(entryDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("Should delete an entry.")
    public void shouldDeleteEntry() throws Exception {
        Mockito.doNothing().when(entryService).deleteEntry("entry_id");
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/entries/entry_id"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    // testing validation
    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("Validation tests: Should set an error for a null user.")
    public void shouldSetErrorForNullUser() throws Exception {
        EntryDto entryDto = new EntryDto("id1", "title", "summary", "content", new Date(), null);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/entries/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonStringEmitFields(entryDto, List.of("userPresentationModel")))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors", Matchers.hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors", Matchers.contains(EntryValidationErrorCodes.ENTRY_AUTHOR_NULL)));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("Validation tests: Should set errors for missing userID and username.")
    public void shouldSetErrorForMissingUserIdAndUserName() throws Exception {
        EntryDto entryDto = new EntryDto("id1", "title", "summary", "content", new Date(), new UserDto());
        mockMvc.perform(MockMvcRequestBuilders.post("/api/entries/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(entryDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors", Matchers.hasSize(4)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors", Matchers.containsInAnyOrder(UserValidationErrorCodes.USER_ID_EMPTY,
                        UserValidationErrorCodes.USER_ID_NULL, UserValidationErrorCodes.USER_NAME_NULL, UserValidationErrorCodes.USER_NAME_EMPTY)));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("Validation tests: Should set errors for missing entry fields.")
    public void shouldSetErrorForMissingEntryFields() throws Exception {
        EntryDto entryDto = new EntryDto();
        mockMvc.perform(MockMvcRequestBuilders.post("/api/entries/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(entryDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors", Matchers.hasSize(5)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors", Matchers.containsInAnyOrder(EntryValidationErrorCodes.ENTRY_CONTENT_NULL,
                        EntryValidationErrorCodes.ENTRY_TITLE_NULL, EntryValidationErrorCodes.ENTRY_SUMMARY_NULL, EntryValidationErrorCodes.ENTRY_CREATION_DATE_NULL,
                        EntryValidationErrorCodes.ENTRY_AUTHOR_NULL)));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("Validation tests: Should set errors for invalid entry fields.")
    public void shouldSetErrorForInvalidEntryFields() throws Exception {
        String title = randStringGenerator(101);
        String summary = randStringGenerator(201);
        String content = randStringGenerator(2001);
        UserDto userDto = new UserDto("uid", "username", Stream.of(new Authority[]{Authority.ROLE_USER}).collect(Collectors.toList()));
        EntryDto entryDto = new EntryDto(null, title, summary, content, new Date(), userDto);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/entries/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(entryDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors", Matchers.hasSize(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors", Matchers.containsInAnyOrder(EntryValidationErrorCodes.ENTRY_CONTENT_INVALID_SIZE,
                        EntryValidationErrorCodes.ENTRY_SUMMARY_INVALID_SIZE, EntryValidationErrorCodes.ENTRY_TITLE_INVALID_SIZE)));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("Validation tests: Should set an error for invalid date.")
    public void shouldSetErrorForInvalidDate() throws Exception {
        String date = "2018-59-59 12:54";
        UserDto userDto = new UserDto("uid", "username", Stream.of(new Authority[]{Authority.ROLE_USER}).collect(Collectors.toList()));
        EntryDto entryDto = new EntryDto(null, "title", "summary", "content", new Date(), userDto);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/entries/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonStringChangeFields(entryDto, Map.of("creationDate", date)))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors", Matchers.hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors", Matchers.containsInAnyOrder(EntryValidationErrorCodes.ENTRY_CREATION_DATE_INVALID_FORMAT)));
    }

    private static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static String asJsonStringEmitFields(final Object obj, final List<String> fields) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.valueToTree(obj);
            fields.stream().forEach(((ObjectNode) jsonNode)::remove);
            return jsonNode.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static String asJsonStringChangeFields(final Object obj, final Map<String, String> fields) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.valueToTree(obj);
            fields.entrySet().stream().forEach(entry -> ((ObjectNode) jsonNode).put(entry.getKey(), entry.getValue()));
            return jsonNode.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String randStringGenerator(int length) {
        RandomStringGenerator randomStringGenerator =
                new RandomStringGenerator.Builder()
                        .withinRange('0', 'z')
                        .filteredBy(CharacterPredicates.LETTERS, CharacterPredicates.DIGITS)
                        .build();
        return randomStringGenerator.generate(length);
    }
}
