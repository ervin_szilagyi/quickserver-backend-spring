package esz.dev.quickserver.blogEntry;

import esz.dev.quickserver.blogEntry.dto.EntryConverter;
import esz.dev.quickserver.blogEntry.dto.EntryDto;
import esz.dev.quickserver.blogEntry.exception.EntryNotFoundException;
import esz.dev.quickserver.blogEntry.model.Entry;
import esz.dev.quickserver.blogEntry.repository.EntryRepository;
import esz.dev.quickserver.blogEntry.service.EntryService;
import esz.dev.quickserver.security.AuthenticationFacadeInterface;
import esz.dev.quickserver.users.dto.UserDto;
import esz.dev.quickserver.users.exception.UserNotFoundException;
import esz.dev.quickserver.users.model.Authority;
import esz.dev.quickserver.users.model.User;
import esz.dev.quickserver.users.service.UserService;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@DisplayName("EntryService test")
public class EntryServiceTests {

    @InjectMocks
    private EntryService entryService;
    @Mock
    private EntryRepository entryRepositoryMock;
    @Mock
    private AuthenticationFacadeInterface authenticationFacadeMock;
    @Mock
    private UserDetails userDetailsMock;
    @Mock
    private EntryConverter entryConverterMock;
    @Mock
    private UserService userService;

    private User user;
    private UserDto userDto;

    @Before
    public void beforeEach() {
        MockitoAnnotations.initMocks(this);
        user = new User("user-id", "user2", "pw2",
                Stream.of(new Authority[]{Authority.ROLE_USER}).collect(Collectors.toList()), true,
                new GregorianCalendar(2018, 05, 27, 12, 59, 59).getTime());
        userDto = new UserDto(user.getId(), user.getUsername(), user.getAuthorities());
        Mockito.when(userDetailsMock.getUsername()).thenReturn(user.getUsername());
        Mockito.when(authenticationFacadeMock.getAuthentication()).thenReturn(userDetailsMock);
    }

    @Test
    @DisplayName("Should return EntryDto instance by ID.")
    public void shouldReturnEntryDtoById() throws EntryNotFoundException {
        String entryId = "entry-id";
        String title = "title";
        String content = "content";
        String summary = "summary";
        Date date = new GregorianCalendar(2018, 05, 27, 12, 59, 59).getTime();
        Entry entry = new Entry(entryId, user, title, summary, content, date);
        Mockito.when(entryRepositoryMock.findById(entryId)).thenReturn(Optional.of(entry));
        Mockito.when(entryConverterMock.createFromEntity(Mockito.any())).thenReturn(
                new EntryDto(entry.getId(), entry.getTitle(), entry.getSummary(), entry.getContent(), entry.getCreationDate(), null)
        );

        EntryDto entryDto = entryService.getEntryDtoById(entryId);

        Assertions.assertThat(entryDto).isNotNull();
        Assertions.assertThat(entryDto.getId()).isEqualTo(entryId);
        Assertions.assertThat(entryDto.getTitle()).isEqualTo(title);
        Assertions.assertThat(entryDto.getSummary()).isEqualTo(summary);
        Assertions.assertThat(entryDto.getContent()).isEqualTo(content);
        Assertions.assertThat(entryDto.getCreationDate()).isEqualTo(date);
        Mockito.verify(entryRepositoryMock, Mockito.times(1)).findById(entryId);
        Mockito.verifyNoMoreInteractions(entryRepositoryMock);
    }

    @Test(expected = EntryNotFoundException.class)
    @DisplayName("Should return EntryNotFoundException when retrieving entries.")
    public void shouldThrowEntryNotFoundExceptionOnRetrieveEntries() throws EntryNotFoundException {
        String entryId = "entry-id";
        Mockito.when(entryRepositoryMock.findById(entryId)).thenReturn(Optional.empty());

        entryService.getEntryDtoById(entryId);

        Mockito.verify(entryRepositoryMock, Mockito.times(1)).findById(entryId);
        Mockito.verifyNoMoreInteractions(entryRepositoryMock);
    }

    @Test
    @DisplayName("Should return first page of entries.")
    public void shouldReturnFirstPageOfEntries() {
        ArrayList<Entry> entries = generateEntries(25);
        int entriesPerPage = 10;
        int page = 1;
        Mockito.when(entryRepositoryMock.findAllByOrderByCreationDateDesc(PageRequest.of((page - 1) * entriesPerPage, entriesPerPage))).thenReturn(new PageImpl<>(entries.subList(page - 1, entriesPerPage)));

        List<EntryDto> actual = entryService.getEntriesByPage(page);
        Assertions.assertThat(entriesPerPage).isEqualTo(actual.size());
        Mockito.verify(entryRepositoryMock, Mockito.times(1)).findAllByOrderByCreationDateDesc(PageRequest.of(page - 1, entriesPerPage));
        Mockito.verifyNoMoreInteractions(entryRepositoryMock);
    }

    @Test
    @DisplayName("Should return third page of entries.")
    public void shouldReturnThirdPageOfEntries() {
        int entryCount = 25;
        ArrayList<Entry> entries = generateEntries(entryCount);
        int entriesPerPage = 10;
        int page = 3;
        int expectedEntryCount = 5;

        Mockito.when(entryRepositoryMock.findAllByOrderByCreationDateDesc(PageRequest.of((page - 1) * entriesPerPage, entriesPerPage))).thenReturn(new PageImpl<>(entries.subList((page - 1) * entriesPerPage, entryCount)));

        List<EntryDto> actual = entryService.getEntriesByPage(page);
        Assertions.assertThat(expectedEntryCount).isEqualTo(actual.size());
        Mockito.verify(entryRepositoryMock, Mockito.times(1)).findAllByOrderByCreationDateDesc(PageRequest.of((page - 1) * entriesPerPage, entriesPerPage));
        Mockito.verifyNoMoreInteractions(entryRepositoryMock);
    }

    private ArrayList<Entry> generateEntries(int count) {
        ArrayList<Entry> entries = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            String entryId = "entry-id-" + i;
            String title = "title-" + i;
            String content = "content-" + i;
            String summary = "summary-" + i;
            Date date = new GregorianCalendar(2018, 05, 27, 12, 59, 59).getTime();
            entries.add(new Entry(entryId, user, title, summary, content, date));
        }
        return entries;
    }

    @Test
    @DisplayName("Should return no entry.")
    public void shouldReturnNoEntry() {
        int entriesPerPage = 10;
        int page = 4;
        int expectedEntryCount = 0;

        Mockito.when(entryRepositoryMock.findAllByOrderByCreationDateDesc(PageRequest.of((page - 1) * entriesPerPage, entriesPerPage))).thenReturn(new PageImpl<>(new ArrayList<>()));

        List<EntryDto> actual = entryService.getEntriesByPage(page);
        Assertions.assertThat(expectedEntryCount).isEqualTo(actual.size());
        Mockito.verify(entryRepositoryMock, Mockito.times(1)).findAllByOrderByCreationDateDesc(PageRequest.of((page - 1) * entriesPerPage, entriesPerPage));
        Mockito.verifyNoMoreInteractions(entryRepositoryMock);
    }

    @Test
    @DisplayName("Should insert an entry.")
    public void shouldInsertAnEntry() throws UserNotFoundException {
        String entryId = "entry-id";
        String title = "title";
        String content = "content";
        String summary = "summary";
        Date date = new GregorianCalendar(2018, 05, 27, 12, 59, 59).getTime();
        Entry entry = new Entry(entryId, user, title, summary, content, date);
        EntryDto entryDto = new EntryDto(entryId, title, summary, content, date, userDto);
        Mockito.when(entryConverterMock.createFromDto(entryDto)).thenReturn(entry);
        Mockito.when(userService.getUserByUsername(user.getUsername())).thenReturn(user);

        entryService.addEntry(entryDto);

        Mockito.verify(entryRepositoryMock, Mockito.times(1)).insert(Mockito.any(Entry.class));
        Mockito.verifyNoMoreInteractions(entryRepositoryMock);
    }

    @Test
    @DisplayName("Should update an entry.")
    public void shouldUpdateAnEntry() throws EntryNotFoundException {
        String entryId = "entry-id";
        String title = "title";
        String content = "content";
        String summary = "summary";
        Date date = new GregorianCalendar(2018, 05, 27, 12, 59, 59).getTime();
        Entry entry = new Entry(entryId, user, title, summary, content, date);
        EntryDto entryDto = new EntryDto(entryId, title, summary, content, date, userDto);
        Mockito.when(entryRepositoryMock.findById(entryId)).thenReturn(Optional.of(entry));

        entryService.updateEntry(entryDto);

        Mockito.verify(entryRepositoryMock, Mockito.times(1)).save(Mockito.any(Entry.class));
        Mockito.verify(entryRepositoryMock, Mockito.times(1)).findById(entryId);
        Mockito.verifyNoMoreInteractions(entryRepositoryMock);
    }

    @Test(expected = EntryNotFoundException.class)
    @DisplayName("Should throw EntryNotFoundException on update.")
    public void shouldThrowEntryNotFoundExceptionOnUpdate() throws EntryNotFoundException {
        String entryId = "entry-id";
        String title = "title";
        String content = "content";
        String summary = "summary";
        Date date = new GregorianCalendar(2018, 05, 27, 12, 59, 59).getTime();
        EntryDto entryDto = new EntryDto(entryId, title, summary, content, date, userDto);
        Mockito.when(entryRepositoryMock.findById(entryId)).thenReturn(Optional.empty());

        entryService.updateEntry(entryDto);
    }

    @Test
    @DisplayName("Should delete an entry.")
    public void shouldDeleteAnEntry() throws EntryNotFoundException {
        String entryId = "entry-id";
        String title = "title";
        String content = "content";
        String summary = "summary";
        Date date = new GregorianCalendar(2018, 05, 27, 12, 59, 59).getTime();
        Entry entry = new Entry(entryId, user, title, summary, content, date);
        Mockito.when(entryRepositoryMock.findById(entryId)).thenReturn(Optional.of(entry));

        entryService.deleteEntry(entryId);

        Mockito.verify(entryRepositoryMock, Mockito.times(1)).delete(Mockito.any(Entry.class));
        Mockito.verify(entryRepositoryMock, Mockito.times(1)).findById(entryId);
        Mockito.verifyNoMoreInteractions(entryRepositoryMock);
    }

    @Test(expected = EntryNotFoundException.class)
    @DisplayName("Should throw EntryNotFoundException when an entry is deleted.")
    public void shouldThrowEntryNotFoundExceptionOnDelete() throws EntryNotFoundException {
        String entryId = "entry-id";
        Mockito.when(entryRepositoryMock.findById(entryId)).thenReturn(Optional.empty());

        entryService.deleteEntry(entryId);
    }

    @Test
    @DisplayName("Should return the number of existing pages.")
    public void shouldReturnNumberOfPages() {
        Mockito.when(entryRepositoryMock.count()).thenReturn(15L);

        Long actual = entryService.getNumberOfPages();

        Assertions.assertThat(actual.longValue()).isEqualTo(1);
        Mockito.verify(entryRepositoryMock, Mockito.times(1)).count();
        Mockito.verifyNoMoreInteractions(entryRepositoryMock);
    }
}
