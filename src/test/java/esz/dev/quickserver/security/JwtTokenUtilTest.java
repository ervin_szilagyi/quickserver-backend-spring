package esz.dev.quickserver.security;

import esz.dev.quickserver.config.ConfigProperties;
import esz.dev.quickserver.security.jwt.JwtClock;
import esz.dev.quickserver.security.jwt.JwtTokenUtil;
import esz.dev.quickserver.security.jwt.dto.JwtAuthenticationResponse;
import esz.dev.quickserver.security.jwt.dto.JwtUser;
import io.jsonwebtoken.ExpiredJwtException;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.DateUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@DisplayName("JwtTokenUtil tests")
public class JwtTokenUtilTest {
    private static final String TEST_USERNAME = "testUser";

    @Mock
    private JwtClock jwtClock;

    @Mock
    private ConfigProperties configProperties;

    @InjectMocks
    private JwtTokenUtil jwtTokenUtil;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigProperties.Jwt jwt = new ConfigProperties.Jwt();
        jwt.setExpiration(3600L);
        jwt.setSecret("mySecret");
        when(configProperties.getJwt()).thenReturn(jwt);
    }

    @Test
    @DisplayName("Should get username from token.")
    public void shouldGetUsernameFromToken() {
        when(jwtClock.now()).thenReturn(DateUtil.now());
        final String token = createToken().getToken();

        final String username = jwtTokenUtil.getUsernameFromToken(token);

        Assertions.assertThat(username).isEqualToIgnoringCase(TEST_USERNAME);
    }

    @Test
    @DisplayName("Should get issued date from token.")
    public void shouldGetIssuedDateFromToken() {
        when(jwtClock.now()).thenReturn(DateUtil.now());
        final Date now = DateUtil.now();
        final String token = createToken().getToken();

        final Date issuedDate = jwtTokenUtil.getIssuedAtDateFromToken(token);

        Assertions.assertThat(Math.abs(now.getTime() - issuedDate.getTime())).isLessThan(1000);
    }

    @Test
    @DisplayName("Should get expiration date from token.")
    public void shouldGetExpirationDateFromToken() {
        when(jwtClock.now()).thenReturn(DateUtil.now());
        final Date now = DateUtil.now();
        final String token = createToken().getToken();

        final Date expirationDate = jwtTokenUtil.getExpirationDateFromToken(token);

        Assertions.assertThat((Math.abs(now.getTime() - expirationDate.getTime()) - 3600000L)).isLessThan(1000);
    }

    @Test(expected = ExpiredJwtException.class)
    @DisplayName("Should throw ExpiredJwtException when token is checked if it can be refreshed.")
    public void shouldTrowExpiredJwtExceptionOnCanTokenBeRefreshed() {
        when(jwtClock.now()).thenReturn(DateUtil.yesterday());
        String token = createToken().getToken();

        jwtTokenUtil.canTokenBeRefreshed(token, DateUtil.tomorrow());
    }

    @Test
    @DisplayName("Should not be able to refresh token.")
    public void shouldNotBeAbleToRefreshToken() {
        when(jwtClock.now()).thenReturn(DateUtil.now());
        String token = createToken().getToken();

        boolean actual = jwtTokenUtil.canTokenBeRefreshed(token, DateUtil.now());

        Assertions.assertThat(actual).isFalse();
    }

    @Test
    @DisplayName("Should be able to refresh token.")
    public void shouldBeAbleToRefreshToken() {
        when(jwtClock.now()).thenReturn(DateUtil.now());
        String token = createToken().getToken();

        boolean actual = jwtTokenUtil.canTokenBeRefreshed(token, DateUtil.yesterday());

        Assertions.assertThat(actual).isTrue();
    }

    @Test
    @DisplayName("Should validate token.")
    public void shouldValidateToken() {
        when(jwtClock.now()).thenReturn(DateUtil.now());
        UserDetails userDetails = mock(JwtUser.class);
        when(((JwtUser) userDetails).getLastPasswordResetDate())
                .thenReturn(new GregorianCalendar(1970, Calendar.JANUARY, 1).getTime());
        when(userDetails.getUsername()).thenReturn(TEST_USERNAME);
        String token = createToken().getToken();

        boolean actual = jwtTokenUtil.validateToken(token, userDetails);

        Assertions.assertThat(actual).isTrue();
    }

    private JwtAuthenticationResponse createToken() {
        return jwtTokenUtil.generateToken(new UserDetailsDummy(TEST_USERNAME));
    }
}
