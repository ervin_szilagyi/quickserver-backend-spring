package esz.dev.quickserver.generic;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface GenericConverter<D extends AbstractDto, E extends BaseEntity> {
    E createFromDto(D dto);

    D createFromEntity(E entity);

    E updateEntity(E entity, D dto);

    default List<D> createFromEntities(final Collection<E> entities) {
        return entities.stream()
                .map(this::createFromEntity)
                .collect(Collectors.toList());
    }

    default List<E> createFromDtos(final Collection<D> dtos) {
        return dtos.stream()
                .map(this::createFromDto)
                .collect(Collectors.toList());
    }
}
