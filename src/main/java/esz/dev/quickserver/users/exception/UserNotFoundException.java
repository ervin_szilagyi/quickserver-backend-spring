package esz.dev.quickserver.users.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such user with id")
public class UserNotFoundException  extends Exception {
}
