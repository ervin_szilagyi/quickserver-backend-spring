package esz.dev.quickserver.users.model;

import esz.dev.quickserver.generic.BaseEntity;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "Users")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class User extends BaseEntity {
    @Id
    private String id;

    @Indexed(unique = true)
    private String username;

    private String password;
    private List<Authority> authorities;
    private boolean enabled;
    private Date lastPasswordResetDate;
}
