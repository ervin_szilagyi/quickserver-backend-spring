package esz.dev.quickserver.users.dto;

import esz.dev.quickserver.generic.GenericConverter;
import esz.dev.quickserver.users.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserConverter implements GenericConverter<UserDto, User> {
    private ModelMapper modelMapper;

    @Autowired
    public UserConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public User createFromDto(UserDto userDto) {
        return modelMapper.map(userDto, User.class);
    }

    @Override
    public UserDto createFromEntity(User user) {
        return modelMapper.map(user, UserDto.class);
    }

    @Override
    public User updateEntity(User entity, UserDto dto) {
        return null;
    }
}
