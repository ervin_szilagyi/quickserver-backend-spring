package esz.dev.quickserver.users.dto;

import esz.dev.quickserver.generic.AbstractDto;
import esz.dev.quickserver.users.model.Authority;
import esz.dev.quickserver.users.validation.UserValidationErrorCodes;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserDto extends AbstractDto {
    @NotNull(message = UserValidationErrorCodes.USER_ID_NULL)
    @NotEmpty(message = UserValidationErrorCodes.USER_ID_EMPTY)
    private String id;

    @NotNull(message = UserValidationErrorCodes.USER_NAME_NULL)
    @NotEmpty(message = UserValidationErrorCodes.USER_NAME_EMPTY)
    private String username;

    private List<Authority> roles;

}
