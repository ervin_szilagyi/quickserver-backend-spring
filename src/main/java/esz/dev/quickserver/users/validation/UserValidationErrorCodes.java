package esz.dev.quickserver.users.validation;

public class UserValidationErrorCodes {
    public static final String USER_ID_NULL = "user.id.null";
    public static final String USER_ID_EMPTY = "user.id.empty";
    public static final String USER_NAME_NULL = "user.name.null";
    public static final String USER_NAME_EMPTY = "user.name.empty";
}
