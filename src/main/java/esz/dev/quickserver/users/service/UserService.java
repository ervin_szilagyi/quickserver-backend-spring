package esz.dev.quickserver.users.service;

import esz.dev.quickserver.users.dto.UserConverter;
import esz.dev.quickserver.users.dto.UserDto;
import esz.dev.quickserver.users.exception.UserNotFoundException;
import esz.dev.quickserver.users.model.User;
import esz.dev.quickserver.users.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

@Service
public class UserService {
    private UserRepository userRepository;
    private UserConverter userConverter;

    @Autowired
    public UserService(UserRepository userRepository, UserConverter userConverter) {
        this.userRepository = userRepository;
        this.userConverter = userConverter;
    }

    public User getUserById(@NotNull String id) throws UserNotFoundException {
        return userRepository.findById(id).orElseThrow(UserNotFoundException::new);
    }

    public UserDto getUserDtoById(@NotNull String id) throws UserNotFoundException {
        return userConverter.createFromEntity(getUserById(id));
    }

    public User getUserByUsername(@NotNull String username) throws UserNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(UserNotFoundException::new);
    }
}
