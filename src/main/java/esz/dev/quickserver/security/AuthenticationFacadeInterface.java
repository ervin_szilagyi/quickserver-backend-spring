package esz.dev.quickserver.security;

import org.springframework.security.core.userdetails.UserDetails;

public interface AuthenticationFacadeInterface {
    UserDetails getAuthentication();
}
