package esz.dev.quickserver.security.jwt;

import esz.dev.quickserver.security.AuthenticationFacadeInterface;
import esz.dev.quickserver.security.jwt.dto.JwtUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationFacade implements AuthenticationFacadeInterface {

    @Autowired
    public JwtAuthenticationFacade() {
    }

    @Override
    public JwtUser getAuthentication() {
        return (JwtUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
