package esz.dev.quickserver.security.jwt.filters;

import esz.dev.quickserver.config.ConfigProperties;
import esz.dev.quickserver.logging.service.LogService;
import esz.dev.quickserver.security.jwt.JwtTokenUtil;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthorizationTokenFilter extends OncePerRequestFilter {
    private UserDetailsService userDetailsService;
    private JwtTokenUtil jwtTokenUtil;
    private LogService logService;
    private ConfigProperties configProperties;

    public JwtAuthorizationTokenFilter(@Qualifier("jwtUserDetailsService") UserDetailsService userDetailsService,
                                       ConfigProperties configProperties,
                                       JwtTokenUtil jwtTokenUtil,
                                       LogService logService) {
        this.userDetailsService = userDetailsService;
        this.configProperties = configProperties;
        this.jwtTokenUtil = jwtTokenUtil;
        this.logService = logService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {
        final String requestHeader = httpServletRequest.getHeader(configProperties.getJwt().getHeader());

        String username = null;
        String authToken = null;
        if (requestHeader != null && requestHeader.startsWith("Bearer ")) {
            authToken = requestHeader.substring(7);
            try {
                username = jwtTokenUtil.getUsernameFromToken(authToken);
            } catch (IllegalArgumentException e) {
                logService.error("An an error occured during getting username from token:" + e.getMessage());
            } catch (ExpiredJwtException e) {
                logService.error("The token is expired and not valid anymore:" + e.getMessage());
            }
        } else {
            logService.info("Couldn't find bearer string, will ignore the header.");
        }

        logService.info("Checking authentication for user " + username);
        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            logService.info("Security context was null, so authorizating user.");

            UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

            if (jwtTokenUtil.validateToken(authToken, userDetails)) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(authentication);
                logService.info(new StringBuilder("User ").append(username).append(" authenticated!").toString());
            } else {
                logService.warning("Invalid token for user: " + username);
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
