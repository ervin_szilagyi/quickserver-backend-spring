package esz.dev.quickserver.security.jwt.controller;

import esz.dev.quickserver.config.ConfigProperties;
import esz.dev.quickserver.security.jwt.JwtTokenUtil;
import esz.dev.quickserver.security.jwt.controller.exception.BadRequestException;
import esz.dev.quickserver.security.jwt.dto.JwtAuthenticationRequest;
import esz.dev.quickserver.security.jwt.dto.JwtAuthenticationResponse;
import esz.dev.quickserver.security.jwt.dto.JwtUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@RestController
@RequestMapping("/api")
public class AuthenticationRestController {
    private AuthenticationManager authenticationManager;
    private UserDetailsService userDetailsService;
    private JwtTokenUtil jwtTokenUtil;
    private ConfigProperties configProperties;

    @Autowired
    public AuthenticationRestController(AuthenticationManager authenticationManager,
                                        @Qualifier("jwtUserDetailsService") UserDetailsService userDetailsService,
                                        ConfigProperties configProperties,
                                        JwtTokenUtil jwtTokenUtil) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.configProperties = configProperties;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @PostMapping("/authenticate")
    @ResponseStatus(HttpStatus.OK)
    public JwtAuthenticationResponse createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest)
            throws DisabledException, BadCredentialsException {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        // Reload password post-security so we can generate the token
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        return jwtTokenUtil.generateToken(userDetails);
    }

    @GetMapping("/refresh")
    @ResponseStatus(HttpStatus.OK)
    public JwtAuthenticationResponse refreshAndGetAuthenticationToken(HttpServletRequest httpServletRequest) throws BadRequestException{
        String authToken = httpServletRequest.getHeader(configProperties.getJwt().getHeader());
        final String token = authToken.substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser jwtUser = (JwtUser) userDetailsService.loadUserByUsername(username);

        if (jwtTokenUtil.canTokenBeRefreshed(token, jwtUser.getLastPasswordResetDate())) {
            return jwtTokenUtil.refreshToken(token);
        }
        throw new BadRequestException();
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler({BadCredentialsException.class})
    public String handleAuthenticationException() {
        return "Bad credentials!";
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler({DisabledException.class})
    public String handleDisabledException() {
        return "User is disabled";
    }

    private void authenticate(String username, String password) throws DisabledException, BadCredentialsException {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    }
}
