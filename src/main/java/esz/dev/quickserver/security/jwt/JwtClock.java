package esz.dev.quickserver.security.jwt;

import io.jsonwebtoken.Clock;
import io.jsonwebtoken.impl.DefaultClock;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtClock {
    private Clock clock = DefaultClock.INSTANCE;

    public Date now() {
        return clock.now();
    }
}
