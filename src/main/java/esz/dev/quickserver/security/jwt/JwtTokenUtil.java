package esz.dev.quickserver.security.jwt;

import esz.dev.quickserver.config.ConfigProperties;
import esz.dev.quickserver.security.jwt.dto.JwtAuthenticationResponse;
import esz.dev.quickserver.security.jwt.dto.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtTokenUtil {
    private ConfigProperties configProperties;
    private JwtClock jwtClock;

    public JwtTokenUtil(ConfigProperties configProperties, JwtClock jwtClock) {
        this.configProperties = configProperties;
        this.jwtClock = jwtClock;
    }

    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public Date getIssuedAtDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getIssuedAt);
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    public JwtAuthenticationResponse generateToken(UserDetails  userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return doGenerateToken(claims, userDetails.getUsername());
    }

    public Boolean canTokenBeRefreshed(String token, Date lastPasswordReset) {
        final Date created = getIssuedAtDateFromToken(token);
        return !isCreatedBeforeLastPasswordReset(created, lastPasswordReset)
                && (!isTokenExpired(token) || ignoreTokenExpiration(token));
    }

    public JwtAuthenticationResponse refreshToken(String token) {
        final Date createdDate = jwtClock.now();
        final Date expirationDate = calculateExpirationDate(createdDate);

        final Claims claims = getAllClaimsFromToken(token);
        claims.setIssuedAt(createdDate);
        claims.setExpiration(expirationDate);

        return new JwtAuthenticationResponse(
                Jwts.builder()
                .setClaims(claims)
                        .signWith(SignatureAlgorithm.HS512, configProperties.getJwt().getSecret())
                        .compact(), configProperties.getJwt().getExpiration());
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        JwtUser user = (JwtUser) userDetails;
        final String username = getUsernameFromToken(token);
        final Date created = getIssuedAtDateFromToken(token);
        return username.equals(user.getUsername())
                && !isTokenExpired(token)
                && !isCreatedBeforeLastPasswordReset(created, user.getLastPasswordResetDate());
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(configProperties.getJwt().getSecret())
                .parseClaimsJws(token)
                .getBody();
    }

    private Boolean isTokenExpired(String token) {
        Date expirationDate = getExpirationDateFromToken(token);
        return expirationDate.before(jwtClock.now());
    }

    private Boolean ignoreTokenExpiration(String token) {
        return false;
    }

    private Date calculateExpirationDate(Date createdDate) {
        return new Date(createdDate.getTime() + configProperties.getJwt().getExpiration() * 1000);
    }

    private JwtAuthenticationResponse doGenerateToken(Map<String, Object> claims, String subject) {
        final Date createdDate = jwtClock.now();
        final Date expirationDate = calculateExpirationDate(createdDate);

        return new JwtAuthenticationResponse(Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, configProperties.getJwt().getSecret())
                .compact(), configProperties.getJwt().getExpiration());
    }

    private Boolean isCreatedBeforeLastPasswordReset(Date created, Date lastPasswordReset) {
        return (lastPasswordReset != null && created.before(lastPasswordReset));
    }
}
