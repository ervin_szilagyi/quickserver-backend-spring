package esz.dev.quickserver.security.jwt.service;

import esz.dev.quickserver.security.jwt.JwtUserFactory;
import esz.dev.quickserver.users.exception.UserNotFoundException;
import esz.dev.quickserver.users.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {
    private UserService userService;

    @Autowired
    public JwtUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            return JwtUserFactory.create(userService.getUserByUsername(username));
        } catch (UserNotFoundException e) {
            throw new UsernameNotFoundException("No user found with name of " + username);
        }
    }
}
