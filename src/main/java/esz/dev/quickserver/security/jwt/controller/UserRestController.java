package esz.dev.quickserver.security.jwt.controller;

import esz.dev.quickserver.config.ConfigProperties;
import esz.dev.quickserver.security.jwt.JwtTokenUtil;
import esz.dev.quickserver.security.jwt.dto.JwtUser;
import esz.dev.quickserver.users.dto.UserDto;
import esz.dev.quickserver.users.model.Authority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/authentication/")
public class UserRestController {
    private UserDetailsService userDetailsService;
    private JwtTokenUtil jwtTokenUtil;
    private ConfigProperties configProperties;

    @Autowired
    public UserRestController(@Qualifier("jwtUserDetailsService") UserDetailsService userDetailsService,
                              JwtTokenUtil jwtTokenUtil, ConfigProperties configProperties) {
        this.userDetailsService = userDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.configProperties = configProperties;
    }

    @GetMapping(value = "user")
    public ResponseEntity<UserDto> getAuthenticatedUser(HttpServletRequest request) {
        String token = request.getHeader(configProperties.getJwt().getHeader()).substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser jwtUser = (JwtUser) userDetailsService.loadUserByUsername(username);
        List<Authority> authorityList = jwtUser.getAuthorities().stream()
                .map(authority -> Authority.valueOf(authority.getAuthority())).collect(Collectors.toList());
        return ResponseEntity.ok().body(new UserDto(jwtUser.getId(), jwtUser.getUsername(), authorityList));
    }
}
