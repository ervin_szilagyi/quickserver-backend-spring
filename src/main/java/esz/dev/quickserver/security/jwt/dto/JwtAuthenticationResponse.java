package esz.dev.quickserver.security.jwt.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JwtAuthenticationResponse {
    private final String token;
    private final Long expiresIn;
}
