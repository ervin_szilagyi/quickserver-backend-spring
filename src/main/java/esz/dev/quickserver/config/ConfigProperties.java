package esz.dev.quickserver.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "app")
public class ConfigProperties {
    private Site site;
    private Jwt jwt;
    private Storage storage;

    @Getter
    @Setter
    public static class Site {
        public String baseuri;
    }

    @Getter
    @Setter
    public static class Jwt {
        public String header;
        public String secret;
        public long expiration;
    }

    @Getter
    @Setter
    public static class Storage {
        public String images;
        public int maxsize;
    }
}
