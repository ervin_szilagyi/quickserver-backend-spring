package esz.dev.quickserver.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GenericConfig {
    @Bean
    ModelMapper getModelMapper() {
        return new ModelMapper();
    }
}
