package esz.dev.quickserver.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket entriesApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("entries")
                .select()
                .apis(RequestHandlerSelectors.basePackage("esz.dev.quickserver.blogEntry"))
                .paths(Predicates.or(PathSelectors.regex("/api/entries.*"),
                        PathSelectors.regex("/api/images.*")))
                .build();
    }

    @Bean
    public Docket statsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("admin")
                .select()
                .apis(Predicates.or(RequestHandlerSelectors.basePackage("esz.dev.quickserver.stats"),
                        RequestHandlerSelectors.basePackage("esz.dev.quickserver.logging")))
                .paths(Predicates.or(PathSelectors.regex("/api/stats/.*"),
                        PathSelectors.regex("/api/logs.*")))
                .build();
    }
}
