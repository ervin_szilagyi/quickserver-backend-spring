package esz.dev.quickserver;

import esz.dev.quickserver.blogEntry.model.Entry;
import esz.dev.quickserver.blogEntry.repository.EntryRepository;
import esz.dev.quickserver.logging.repository.LogRepository;
import esz.dev.quickserver.users.model.Authority;
import esz.dev.quickserver.users.model.User;
import esz.dev.quickserver.users.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootApplication
public class QuickserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuickserverApplication.class, args);
    }

    @Bean
    @Profile("development")
    CommandLineRunner initialize(EntryRepository entryRepository,
                                 UserRepository userRepository,
                                 LogRepository logRepository) {
        entryRepository.deleteAll();
        userRepository.deleteAll();
        logRepository.deleteAll();


        return args -> {
            User admin = new User(UUID.randomUUID().toString(), "admin", new BCryptPasswordEncoder().encode("admin"),
                    Stream.of(new Authority[]{Authority.ROLE_ADMIN}).collect(Collectors.toList()), true, new GregorianCalendar(1970, Calendar.JANUARY, 1).getTime());
            User user = new User(UUID.randomUUID().toString(), "user", new BCryptPasswordEncoder().encode("user"),
                    Stream.of(new Authority[]{Authority.ROLE_USER}).collect(Collectors.toList()), true, new Date());
            User[] users = new User[]{admin, user};
            Stream.of(users).forEach(userRepository::save);
            Entry[] entries = new Entry[]{
                    new Entry(UUID.randomUUID().toString(), admin, "title1", "summary1", "content1", new Date()),
                    new Entry(UUID.randomUUID().toString(), admin, "title2", "summary2", "content2", new Date()),
                    new Entry(UUID.randomUUID().toString(), user, "title3", "summary3", "content3", new Date()),
                    new Entry(UUID.randomUUID().toString(), admin, "title4", "summary4", "content4", new Date()),
                    new Entry(UUID.randomUUID().toString(), admin, "title5", "summary5", "content5", new Date()),
                    new Entry(UUID.randomUUID().toString(), user, "title6", "summary6", "content6", new Date()),
                    new Entry(UUID.randomUUID().toString(), user, "title7", "summary7", "content7", new Date()),
                    new Entry(UUID.randomUUID().toString(), admin, "title8", "summary8", "content8", new Date())
            };
            Stream.of(entries).forEach(entryRepository::save);
        };
    }
}
