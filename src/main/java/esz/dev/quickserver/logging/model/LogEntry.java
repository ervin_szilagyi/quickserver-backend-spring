package esz.dev.quickserver.logging.model;

import esz.dev.quickserver.generic.BaseEntity;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "Logs")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class LogEntry extends BaseEntity {
    @Id
    protected String id;

    @Indexed(direction = IndexDirection.DESCENDING)
    protected Date creationDate;

    protected Level level;
    protected String message;
}
