package esz.dev.quickserver.logging.model;

public enum Level {
    INFO, WARNING, ERROR, CRITICAL
}
