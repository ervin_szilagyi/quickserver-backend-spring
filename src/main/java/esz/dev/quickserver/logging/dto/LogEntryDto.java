package esz.dev.quickserver.logging.dto;

import esz.dev.quickserver.generic.AbstractDto;
import esz.dev.quickserver.logging.model.Level;
import lombok.*;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class LogEntryDto extends AbstractDto {
    protected String id;
    protected Date creationDate;

    protected Level level;
    protected String message;
}
