package esz.dev.quickserver.logging.dto;

import esz.dev.quickserver.generic.GenericConverter;
import esz.dev.quickserver.logging.model.LogEntry;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LogEntryConverter implements GenericConverter<LogEntryDto, LogEntry> {
    private ModelMapper modelMapper;

    @Autowired
    public LogEntryConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public LogEntry createFromDto(LogEntryDto logEntryDto) {
        return modelMapper.map(logEntryDto, LogEntry.class);
    }

    @Override
    public LogEntryDto createFromEntity(LogEntry logEntry) {
        return modelMapper.map(logEntry, LogEntryDto.class);
    }

    @Override
    public LogEntry updateEntity(LogEntry entity, LogEntryDto dto) {
        return null;
    }
}
