package esz.dev.quickserver.logging.repository;

import esz.dev.quickserver.logging.model.LogEntry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LogRepository extends MongoRepository<LogEntry, String> {
    Page<LogEntry> findAllByOrderByCreationDateDesc(Pageable pageable);
}
