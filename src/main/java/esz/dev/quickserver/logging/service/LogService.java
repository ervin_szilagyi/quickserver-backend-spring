package esz.dev.quickserver.logging.service;

import esz.dev.quickserver.logging.dto.LogEntryConverter;
import esz.dev.quickserver.logging.dto.LogEntryDto;
import esz.dev.quickserver.logging.model.Level;
import esz.dev.quickserver.logging.model.LogEntry;
import esz.dev.quickserver.logging.repository.LogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class LogService {
    private static final int ENTRIES_PER_PAGE = 100;
    private LogRepository logRepository;
    private LogEntryConverter logEntryConverter;
    private Logger logStdout = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public LogService(LogRepository logRepository, LogEntryConverter logEntryConverter) {
        this.logRepository = logRepository;
        this.logEntryConverter = logEntryConverter;
    }

    private void log(Level level, String message) {
        var creationDate = new Date();
        var id = UUID.randomUUID().toString();
        var logEntry = new LogEntry(id, creationDate, level, message);
        logRepository.insert(logEntry);
    }

    public void info(String message) {
        log(Level.INFO, message);
        logStdout.info(message);
    }

    public void warning(String message) {
        log(Level.WARNING, message);
        logStdout.warn(message);
    }

    public void error(String message) {
        log(Level.ERROR, message);
        logStdout.error(message);
    }

    public void critical(String message) {
        log(Level.CRITICAL, message);
        logStdout.error(message);
    }

    public List<LogEntryDto> getLogsByPage(Integer page) {
        int startPosition = ENTRIES_PER_PAGE * (page - 1);
        return logEntryConverter.createFromEntities(
                logRepository.findAllByOrderByCreationDateDesc(
                        PageRequest.of(startPosition, ENTRIES_PER_PAGE)).getContent());
    }
}

