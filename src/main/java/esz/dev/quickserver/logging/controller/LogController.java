package esz.dev.quickserver.logging.controller;

import esz.dev.quickserver.logging.dto.LogEntryDto;
import esz.dev.quickserver.logging.service.LogService;
import esz.dev.quickserver.users.model.Authority;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(consumes = "application/json", produces = "application/json")
@RestController
@RequestMapping("/api/logs")
public class LogController {
    private LogService logService;

    @Autowired
    public LogController(LogService logService) {
        this.logService = logService;
    }

    @ApiOperation("Gets the first N number of log entries.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The logs were retrieved successfully."),
            }
    )
    @GetMapping(value = "/page/{page}", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('" + Authority.ADMIN + "') OR hasRole('" + Authority.USER + "')")
    @ResponseStatus(HttpStatus.OK)
    public List<LogEntryDto> getLogsByPage(
            @ApiParam(value = "An integer bigger than 0 which specifies the current page number.", required = true)
            @PathVariable Integer page
    ) {
        return logService.getLogsByPage(page);
    }
}
