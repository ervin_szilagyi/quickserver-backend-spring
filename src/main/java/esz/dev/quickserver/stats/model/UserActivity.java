package esz.dev.quickserver.stats.model;

import lombok.*;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class UserActivity {
    private String userId;
    private String userName;
    private Date latestActivity;
}
