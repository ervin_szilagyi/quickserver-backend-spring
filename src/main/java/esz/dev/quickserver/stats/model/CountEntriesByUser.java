package esz.dev.quickserver.stats.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class CountEntriesByUser {
    private String authorId;
    private String authorName;
    private Integer nrEntries;
}
