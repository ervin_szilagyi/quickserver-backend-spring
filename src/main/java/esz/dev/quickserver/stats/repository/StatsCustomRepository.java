package esz.dev.quickserver.stats.repository;

import esz.dev.quickserver.stats.model.CountEntriesByUser;
import esz.dev.quickserver.stats.model.UserActivity;

import java.util.List;

public interface StatsCustomRepository {
    List<CountEntriesByUser> getNumberOfPostsFromAuthors();
    List<UserActivity> getUserActivity();
}
