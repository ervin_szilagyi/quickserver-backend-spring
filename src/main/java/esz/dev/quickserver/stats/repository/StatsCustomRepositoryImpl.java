package esz.dev.quickserver.stats.repository;

import esz.dev.quickserver.blogEntry.model.Entry;
import esz.dev.quickserver.stats.model.CountEntriesByUser;
import esz.dev.quickserver.stats.model.UserActivity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StatsCustomRepositoryImpl implements StatsCustomRepository {
    private MongoTemplate mongoTemplate;

    @Autowired
    public StatsCustomRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public List<CountEntriesByUser> getNumberOfPostsFromAuthors() {
        GroupOperation groupOperation = Aggregation.group("authorId").count().as("nrEntries");
        SortOperation sortOperation = Aggregation.sort(new Sort(Sort.Direction.DESC, "nrEntries"));
        LookupOperation lookupOperation = Aggregation.lookup("Users", "_id", "_id", "user");
        ProjectionOperation projectionOperation = Aggregation.project()
                .andExpression("_id").as("authorId")
                .andExpression("user.username").as("authorName")
                .andExpression("nrEntries").as("nrEntries");
        Aggregation aggregation =  Aggregation.newAggregation(groupOperation, sortOperation, lookupOperation, projectionOperation);
        AggregationResults<CountEntriesByUser> results =
                mongoTemplate.aggregate(aggregation, Entry.class, CountEntriesByUser.class);
        return results.getMappedResults();
    }

    @Override
    public List<UserActivity> getUserActivity() {
        GroupOperation groupOperation = Aggregation.group("authorId").push("creationDate").as("creationDate");
        SortOperation sortOperation = Aggregation.sort(new Sort(Sort.Direction.DESC, "creationDate"));
        LookupOperation lookupOperation = Aggregation.lookup("Users", "_id", "_id", "user");
        ProjectionOperation projectionOperation = Aggregation.project("authorId")
                .andExpression("_id").as("userId")
                .andExpression("user.username").as("userName")
                .andExpression("creationDate").as("latestActivity");
        Aggregation aggregation =  Aggregation.newAggregation(groupOperation, sortOperation, lookupOperation, projectionOperation);
        AggregationResults<UserActivity> results =
                mongoTemplate.aggregate(aggregation, Entry.class, UserActivity.class);
        return results.getMappedResults();
    }
}
