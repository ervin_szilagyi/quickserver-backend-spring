package esz.dev.quickserver.stats.controller;

import esz.dev.quickserver.stats.model.CountEntriesByUser;
import esz.dev.quickserver.stats.model.UserActivity;
import esz.dev.quickserver.stats.service.StatsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(consumes = "application/json", produces = "application/json")
@RestController
@RequestMapping("/api/stats")
public class StatsRestController {

    private StatsService statsService;

    @Autowired
    public StatsRestController(StatsService statsService) {
        this.statsService = statsService;
    }

    @ApiOperation(value = "Returns number of blog entries for each author.",
            notes = "Admin role is necessary for being able to use this service.",
            response = CountEntriesByUser.class,
            responseContainer = "List",
            authorizations = {
                    @Authorization(value = "ROLE_ADMIN")
            })
    @GetMapping(value = "/entriesByAuthor", produces = MediaType.APPLICATION_PROBLEM_JSON_VALUE)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<CountEntriesByUser> getNrOfEntriesByAuthor() {
        return statsService.getNumberOfPostsFromAuthors();
    }

    @ApiOperation(value = "Returns the timestamp of the latest activity for each user.",
            notes = "Admin role is necessary for being able to use this service.",
            response = UserActivity.class,
            responseContainer = "List",
            authorizations = {
                @Authorization(value = "jwt")
            })
    @GetMapping(value = "/userActivity", produces = MediaType.APPLICATION_PROBLEM_JSON_VALUE)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<UserActivity> getUserActivity() {
        return statsService.getUserActivity();
    }
}
