package esz.dev.quickserver.stats.service;

import esz.dev.quickserver.stats.repository.StatsCustomRepository;
import esz.dev.quickserver.stats.model.CountEntriesByUser;
import esz.dev.quickserver.stats.model.UserActivity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatsService {

    private StatsCustomRepository statsCustomRepository;

    @Autowired
    public StatsService(StatsCustomRepository statsCustomRepository) {
        this.statsCustomRepository = statsCustomRepository;
    }

    @Secured({"ROLE_ADMIN"})
    public List<CountEntriesByUser> getNumberOfPostsFromAuthors() {
        return statsCustomRepository.getNumberOfPostsFromAuthors();
    }

    @Secured({"ROLE_ADMIN"})
    public List<UserActivity> getUserActivity() {
        return statsCustomRepository.getUserActivity();
    }
}
