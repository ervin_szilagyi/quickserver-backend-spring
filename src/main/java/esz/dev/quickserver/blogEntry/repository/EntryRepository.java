package esz.dev.quickserver.blogEntry.repository;

import esz.dev.quickserver.blogEntry.model.Entry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EntryRepository extends MongoRepository<Entry, String> {
    Page<Entry> findAllByOrderByCreationDateDesc(Pageable pageable);
}
