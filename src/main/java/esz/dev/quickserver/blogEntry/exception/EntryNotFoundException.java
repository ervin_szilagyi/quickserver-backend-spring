package esz.dev.quickserver.blogEntry.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such entry with id")
public class EntryNotFoundException extends Exception {
    //
}
