package esz.dev.quickserver.blogEntry.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Upload failed")
public class UploadFailedException extends Exception {
}
