package esz.dev.quickserver.blogEntry.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "File size is too big.")
public class FileSizeException extends Exception {
}
