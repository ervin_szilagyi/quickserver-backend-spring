package esz.dev.quickserver.blogEntry.dto;

import esz.dev.quickserver.blogEntry.validation.EntryValidationErrorCodes;
import esz.dev.quickserver.generic.AbstractDto;
import esz.dev.quickserver.users.dto.UserDto;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class EntryDto extends AbstractDto {
    private String id;

    @NotNull(message = EntryValidationErrorCodes.ENTRY_TITLE_NULL)
    @Size(min = 1, max = 100, message = EntryValidationErrorCodes.ENTRY_TITLE_INVALID_SIZE)
    private String title;

    @NotNull(message = EntryValidationErrorCodes.ENTRY_SUMMARY_NULL)
    @Size(max = 200, message = EntryValidationErrorCodes.ENTRY_SUMMARY_INVALID_SIZE)
    private String summary;

    @NotNull(message = EntryValidationErrorCodes.ENTRY_CONTENT_NULL)
    @Size(max = 2000, message = EntryValidationErrorCodes.ENTRY_CONTENT_INVALID_SIZE)
    private String content;

    @NotNull(message = EntryValidationErrorCodes.ENTRY_CREATION_DATE_NULL)
    @DateTimeFormat(pattern="dd.mm.yyyy HH:mm")
    private Date creationDate;

    @Valid
    @NotNull(message = EntryValidationErrorCodes.ENTRY_AUTHOR_NULL)
    private UserDto userDto;
}
