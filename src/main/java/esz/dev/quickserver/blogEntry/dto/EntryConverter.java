package esz.dev.quickserver.blogEntry.dto;

import esz.dev.quickserver.blogEntry.model.Entry;
import esz.dev.quickserver.generic.GenericConverter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EntryConverter implements GenericConverter<EntryDto, Entry> {
    private ModelMapper modelMapper;

    @Autowired
    public EntryConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Entry createFromDto(EntryDto entryDto) {
        return modelMapper.map(entryDto, Entry.class);
    }

    @Override
    public EntryDto createFromEntity(Entry entity) {
        return modelMapper.map(entity, EntryDto.class);
    }

    @Override
    public Entry updateEntity(Entry entity, EntryDto dto) {
        return null;
    }
}
