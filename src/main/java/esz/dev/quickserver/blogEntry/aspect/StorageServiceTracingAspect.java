package esz.dev.quickserver.blogEntry.aspect;

import esz.dev.quickserver.logging.service.LogService;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
@Aspect
public class StorageServiceTracingAspect {
    private LogService logService;

    @Autowired
    public StorageServiceTracingAspect(LogService logService) {
        this.logService = logService;
    }

    @AfterThrowing(value = "execution(* esz.dev.quickserver.blogEntry.service.StorageService.getImage(*))", throwing = "ex")
    public void afterGetImageException(Exception ex) {
        String message = "Exception was thrown during the attempt of retrieving an image: " + ex.toString();
        logService.warning(message);
    }

    @AfterReturning(value = "execution(* esz.dev.quickserver.blogEntry.service.StorageService.getImage(*))", returning = "image")
    public void afterGetImageResult(byte[] image) {
        String message = "Image retrieved successfully. Size: " + (image.length * 1024) + "KB";
        logService.info(message);
    }

    @AfterThrowing(value = "execution(* esz.dev.quickserver.blogEntry.service.StorageService.storeImage(*))", throwing = "ex")
    public void afterStoreImageException(Exception ex) {
        String message = "Exception was thrown during the attempt of storing an image: " + ex.toString();
        logService.warning(message);
    }

    @AfterReturning(value = "execution(* esz.dev.quickserver.blogEntry.service.StorageService.storeImage(*))", returning = "multipartFile")
    public void afterStoreImageResult(MultipartFile multipartFile) {
        String message = "Image (filename: " + multipartFile.getOriginalFilename() + " , size:" + (multipartFile.getSize() * 1024) + "KB)";
        logService.info(message);
    }
}
