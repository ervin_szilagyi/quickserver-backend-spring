package esz.dev.quickserver.blogEntry.aspect;

import esz.dev.quickserver.blogEntry.dto.EntryDto;
import esz.dev.quickserver.logging.service.LogService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Aspect
public class EntryServiceTracingAspect {
    private LogService logService;

    @Autowired
    public EntryServiceTracingAspect(LogService logService) {
        this.logService = logService;
    }

    @AfterThrowing(value = "execution(* esz.dev.quickserver.blogEntry.service.EntryService.getEntryById(*))", throwing = "ex")
    public void afterGetEntryException(Exception ex) {
        String message = "Exception was thrown during the attempt of retrieving entries: " + ex.toString();
        logService.warning(message);
    }

    @AfterReturning(value = "execution(* esz.dev.quickserver.blogEntry.service.EntryService.getEntryById(*))", returning = "entry")
    public void afterGetEntryResult(EntryDto entry) {
        String message = "Entry retrieved successfully: " + entry.getId();
        logService.warning(message);
    }

    @AfterReturning(value = "execution(* esz.dev.quickserver.blogEntry.service.EntryService.getEntriesByPage(*))", returning = "listOfEntries")
    public void afterGetEntryException(List<EntryDto> listOfEntries) {
        String message = "Successfully retrieved a page of " + listOfEntries.size() + " entries.";
        logService.info(message);
    }

    @Before(value = "execution(* esz.dev.quickserver.blogEntry.service.EntryService.addEntry(*))")
    public void beforeAddEntry(JoinPoint joinPoint) {
        var entryPresentationModel = (EntryDto)joinPoint.getArgs()[0];
        String message = "Attempting to add blog entry with title: " + entryPresentationModel.getTitle();
        logService.info(message);
    }

    @Before(value = "execution(* esz.dev.quickserver.blogEntry.service.EntryService.updateEntry(*))")
    public void beforeUpdateEntry(JoinPoint joinPoint) {
        var entryPresentationModel = (EntryDto)joinPoint.getArgs()[0];
        String message = "Attempting to update blog entry with id: " + entryPresentationModel.getId();
        logService.info(message);
    }

    @AfterThrowing(value = "execution(* esz.dev.quickserver.blogEntry.service.EntryService.updateEntry(*))", throwing = "ex")
    public void afterUpdateEntryException(Exception ex) {
        String message = "Exception was thrown during the attempt of updating an entry: " + ex.toString();
        logService.warning(message);
    }

    @Before(value = "execution(* esz.dev.quickserver.blogEntry.service.EntryService.deleteEntry(*))")
    public void beforeDeleteEntry(JoinPoint joinPoint) {
        var id = (String)joinPoint.getArgs()[0];
        String message = "Attempting to update delete entry with id: " + id;
        logService.info(message);
    }

    @AfterThrowing(value = "execution(* esz.dev.quickserver.blogEntry.service.EntryService.deleteEntry(*))", throwing = "ex")
    public void afterDeleteEntryException(Exception ex) {
        String message = "Exception was thrown during the attempt of deleting an entry: " + ex.toString();
        logService.warning(message);
    }
}
