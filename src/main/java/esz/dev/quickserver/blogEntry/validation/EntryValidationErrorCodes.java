package esz.dev.quickserver.blogEntry.validation;

public class EntryValidationErrorCodes {
    public static final String ENTRY_TITLE_NULL = "entry.title.null";
    public static final String ENTRY_TITLE_INVALID_SIZE = "entry.title.invalid_size";
    public static final String ENTRY_SUMMARY_INVALID_SIZE = "entry.summary.invalid_size";
    public static final String ENTRY_SUMMARY_NULL = "entry.summary.null";
    public static final String ENTRY_CONTENT_NULL = "entry.content.null";
    public static final String ENTRY_CONTENT_INVALID_SIZE = "entry.content.invalid_size";
    public static final String ENTRY_AUTHOR_NULL = "entry.author.null";
    public static final String ENTRY_CREATION_DATE_NULL = "entry.creation_date.null";
    public static final String ENTRY_CREATION_DATE_INVALID_FORMAT = "entry.creation_date.invalid_format";
}
