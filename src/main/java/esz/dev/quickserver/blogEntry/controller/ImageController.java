package esz.dev.quickserver.blogEntry.controller;

import esz.dev.quickserver.blogEntry.exception.FileSizeException;
import esz.dev.quickserver.blogEntry.service.StorageService;
import esz.dev.quickserver.blogEntry.exception.UploadFailedException;
import esz.dev.quickserver.blogEntry.model.UploadResponse;
import esz.dev.quickserver.users.model.Authority;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Api(consumes = "application/json", produces = "application/json")
@RestController
@RequestMapping("/api/images")
public class ImageController {
    private StorageService storageService;

    @Autowired
    public ImageController(StorageService storageService) {
        this.storageService = storageService;
    }

    @ApiOperation("Gets the image from the storage.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The image was retrieved successfully."),
                    @ApiResponse(code = 404, message = "The image can not be found in our database.")
            }
    )
    @GetMapping(value = "/{filename}")
    public byte[] getImage(
            @ApiParam(value = "A string containing the id of the entry.", required = true)
            @PathVariable String filename) throws IOException {
        return storageService.getImage(filename);
    }

    @ApiOperation("Uploads an image for an entry.")
    @PostMapping(value = "/", produces = MediaType.APPLICATION_PROBLEM_JSON_VALUE, consumes = "multipart/form-data")
    @PreAuthorize("hasRole('" + Authority.ADMIN + "') OR hasRole('" + Authority.USER + "')")
    @ResponseStatus(HttpStatus.OK)
    public UploadResponse uploadImage(@RequestParam("file") MultipartFile multipartFile) throws FileSizeException {
        if (!multipartFile.isEmpty()) {
            try {
                return storageService.storeImage(multipartFile);
            } catch (UploadFailedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = IOException.class)
    public String imageNotFound() {
        return "The image can not be found in our database.";
    }
}
