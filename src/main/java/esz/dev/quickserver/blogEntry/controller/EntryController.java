package esz.dev.quickserver.blogEntry.controller;

import esz.dev.quickserver.blogEntry.dto.EntryDto;
import esz.dev.quickserver.blogEntry.exception.EntryNotFoundException;
import esz.dev.quickserver.blogEntry.service.EntryService;
import esz.dev.quickserver.blogEntry.validation.EntryValidationErrorCodes;
import esz.dev.quickserver.blogEntry.validation.EntryValidationErrorModel;
import esz.dev.quickserver.users.model.Authority;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(consumes = "application/json", produces = "application/json")
@RestController
@RequestMapping("/api/entries")
public class EntryController {
    private EntryService entryService;

    @Autowired
    public EntryController(EntryService entryService) {
        this.entryService = entryService;
    }

    @ApiOperation("Gets the summaries for the entries for the given input page.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The entry for selected page were retrieved successfully."),
            }
    )
    @GetMapping(value = "/page/{page}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<EntryDto> getEntriesByPage(
            @ApiParam(value = "An integer bigger than 0 which specifies the current page number.", required = true)
            @PathVariable Integer page) {
        return entryService.getEntriesByPage(page);
    }

    @ApiOperation("Gets the entire content of an entry specified by the entryId.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "The entry was retrieved successfully."),
                    @ApiResponse(code = 404, message = "The entry with the given id was not found.")
            }
    )
    @GetMapping(value = "/{entryId}", produces = MediaType.APPLICATION_PROBLEM_JSON_VALUE)
    public EntryDto getEntryById(
            @ApiParam(value = "A string containing the id of the entry.", required = true)
            @PathVariable String entryId) throws EntryNotFoundException {
        return entryService.getEntryDtoById(entryId);
    }

    @ApiOperation("Updates an entry with the specified id.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 202, message = "The entry was updated successfully."),
                    @ApiResponse(code = 404, message = "The entry with the given id was not found.")
            }
    )
    @PutMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void updateEntry(@ApiParam(value = "The model of the updated entry. It should have a valid entry id.", required = true)
                            @Valid @RequestBody EntryDto entry) throws EntryNotFoundException {
        entryService.updateEntry(entry);
    }

    @ApiOperation("Inserts a new entry into the database.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "A new entry was created successfully.")
            }
    )
    @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('" + Authority.ADMIN + "') OR hasRole('" + Authority.USER + "')")
    @ResponseStatus(HttpStatus.CREATED)
    public void addEntry(@ApiParam(value = "The model of the newly created entry. The id of the entry should be null.", required = true)
                         @Valid @RequestBody EntryDto entry) {
        entryService.addEntry(entry);
    }

    @ApiOperation("Deletes an entry with the given id from the database.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 204, message = "The entry was deleted successfully."),
                    @ApiResponse(code = 404, message = "The entry with the given id was not found.")
            }
    )
    @DeleteMapping(value = "/{entryId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('" + Authority.ADMIN + "')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEntry(@ApiParam(value = "The id of the entry which will be deleted.", required = true)
                            @PathVariable String entryId) throws EntryNotFoundException {
        entryService.deleteEntry(entryId);
    }

    @ApiOperation("Retrieves the number of pages (number of entries divided by the number of entries per page.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Successfully retrieved the number of pages."),
            }
    )
    @GetMapping(value = "/numberOfPages", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public Long countEntries() {
        return entryService.getNumberOfPages();
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public EntryValidationErrorModel invalidDate(final HttpMessageNotReadableException ex) {
        return new EntryValidationErrorModel(List.of(EntryValidationErrorCodes.ENTRY_CREATION_DATE_INVALID_FORMAT));
    }

}
