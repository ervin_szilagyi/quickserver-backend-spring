package esz.dev.quickserver.blogEntry.model;

import esz.dev.quickserver.generic.BaseEntity;
import esz.dev.quickserver.users.model.User;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "Entries")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class Entry extends BaseEntity {
    @Id
    private String id;

    @DBRef
    private User author;

    private String title;
    private String summary;
    private String content;

    @Indexed(direction = IndexDirection.DESCENDING)
    private Date creationDate;
}
