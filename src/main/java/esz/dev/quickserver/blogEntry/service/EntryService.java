package esz.dev.quickserver.blogEntry.service;

import esz.dev.quickserver.blogEntry.dto.EntryConverter;
import esz.dev.quickserver.blogEntry.dto.EntryDto;
import esz.dev.quickserver.blogEntry.exception.EntryNotFoundException;
import esz.dev.quickserver.blogEntry.model.Entry;
import esz.dev.quickserver.blogEntry.repository.EntryRepository;
import esz.dev.quickserver.security.AuthenticationFacadeInterface;
import esz.dev.quickserver.users.service.UserService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class EntryService {
    private static final int ENTRIES_PER_PAGE = 10;
    private EntryRepository entryRepository;
    private UserService userService;
    private AuthenticationFacadeInterface authenticationFacade;
    private EntryConverter entryConverter;

    @Autowired
    public EntryService(EntryRepository entryRepository, UserService userService,
                        AuthenticationFacadeInterface authenticationFacade, EntryConverter entryConverter) {
        this.entryRepository = entryRepository;
        this.userService = userService;
        this.authenticationFacade = authenticationFacade;
        this.entryConverter = entryConverter;
    }

    public Entry getEntryById(@NotNull String id) throws EntryNotFoundException {
        return entryRepository.findById(id).orElseThrow(EntryNotFoundException::new);
    }

    public EntryDto getEntryDtoById(@NotNull String id) throws EntryNotFoundException {
        Entry entry = getEntryById(id);
        return entryConverter.createFromEntity(entry);
    }

    public List<EntryDto> getEntriesByPage(@NotNull Integer page) {
        int startPosition = ENTRIES_PER_PAGE * (page - 1);
        return entryRepository.findAllByOrderByCreationDateDesc(PageRequest.of(startPosition, ENTRIES_PER_PAGE))
                .getContent().stream()
                .map(entry -> entryConverter.createFromEntity(entry))
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public void addEntry(EntryDto entryDto) {
        var entry = entryConverter.createFromDto(entryDto);
        entry.setId(UUID.randomUUID().toString());
        entry.setAuthor(userService.getUserByUsername(authenticationFacade.getAuthentication().getUsername()));
        entryRepository.insert(entry);
    }

    public void updateEntry(EntryDto entryDto) throws EntryNotFoundException {
        Entry entry = this.getEntryById(entryDto.getId());
        entry.setTitle(entryDto.getTitle());
        entry.setContent(entryDto.getContent());
        entry.setCreationDate(entryDto.getCreationDate());
        entryRepository.save(entry);
    }

    public void deleteEntry(@NotNull String id) throws EntryNotFoundException {
        Entry entry = getEntryById(id);
        entryRepository.delete(entry);
    }

    public Long getNumberOfPages() {
        return entryRepository.count() / ENTRIES_PER_PAGE;
    }
}

