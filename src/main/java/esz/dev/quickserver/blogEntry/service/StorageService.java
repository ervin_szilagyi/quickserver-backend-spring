package esz.dev.quickserver.blogEntry.service;

import esz.dev.quickserver.blogEntry.exception.FileSizeException;
import esz.dev.quickserver.blogEntry.exception.UploadFailedException;
import esz.dev.quickserver.blogEntry.model.UploadResponse;
import esz.dev.quickserver.config.ConfigProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class StorageService {
    private Path rootLocation;
    private ConfigProperties configProperties;

    public StorageService(ConfigProperties configProperties) {
        this.configProperties = configProperties;
        rootLocation = Paths.get(configProperties.getStorage().getImages());
    }

    public byte[] getImage(String filename) throws IOException {
        var file = new File(Paths.get(rootLocation.toString(), filename).toUri());
        return Files.readAllBytes(file.toPath());
    }

    public UploadResponse storeImage(MultipartFile multipartFile) throws UploadFailedException, FileSizeException {
        try {
            if (multipartFile.getSize() > configProperties.getStorage().getMaxsize()) {
                throw new FileSizeException();
            }
            Files.copy(multipartFile.getInputStream(), rootLocation.resolve(multipartFile.getOriginalFilename()));
            return new UploadResponse(configProperties.getSite().getBaseuri() +
                    multipartFile.getOriginalFilename());
        } catch (IOException e) {
            throw new UploadFailedException();
        }
    }
}
