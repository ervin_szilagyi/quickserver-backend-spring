# QuickServer

## Build and deploy (production)

```
mvn compile
mvn package
docker-compose up
```

## Running

`java -Dspring.profiles.active=production -jar app.jar`

## Development

In order to be able to run this application with the `development` profile, you need to have up an running a mongo
instance on localhost port `27017`. The easies way to achieve this is to spawn up a docker container with mongodb:
```
docker image pull mongo:latest
docker container run -p 27017:27017 -d --name mongodb mongo:latest
```

## API descriptions

In order to see API description, open the swagger documentation using the following link:
http://localhost:8080/swagger-ui.html

